---
title: "Presencia web"
date: "2023-05-10"
draft: false
#layout: "simple"
showAuthor: true
---
Leí que es bueno tener una página web personal-profesional, incluso cuando ya se tenga perfil LinkedIn y similares. Sirve para reforzar nuestra marca personal y descentralizarla un poco de las grandes plataformas.
Esta publicación también es una primera prueba de la página web.