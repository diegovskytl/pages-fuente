---
title: "Currículum Vitae"
date: 2023-05-09T10:29:52-06:00
layout: "simple"
---

## Diego Alejandro Reyes Ramos
###  [diego.reyes.ar@proton.me](mailto:diego.reyes.ar@proton.me) · [LinkedIn](https://www.linkedin.com/in/diego-alejandro-reyes-ramos/)


### Experiencia laboral

-   **Investigador para software de simulación de emisiones** · Dinámica Heurística | _enero 2022 – actualidad_
    -   Investigación y mejora de los modelos utilizados en programas de simulación de emisiones y explosiones
    -   Desarrollo de dicho software en .NET Core y Visual Basic
-   **Diseñador y monitor de sistemas fotovoltaicos** · Energon Solar | _enero 2020 – mayo 2021_
    -   Evaluación de inmuebles, diseño tridimensional de instalaciones y simulación de generación energética
    -   Monitoreo de integridad, generación de energía y mantenimiento de sistemas fotovoltaicos en EnnexOS


### Participaciones académicas extracurriculares

-   **Investigador y redactor** · Federación Interuniversitaria para el Desarrollo Sostenible | _nov. 2019 – jun. 2020_
    -   Desarrollo de propuestas de sostenibilidad para presentar ante el Fomento Energético de Nuevo León
-   **Coordinador y gestor de patrocinios** · Student Energy at Tec de Monterrey | _marzo 2019 – diciembre 2019_
    -   Negociación de patrocinios para financiar eventos y viajes de estudio enfocados en sustentabilidad energética
-   **Colaborador en logística** · MIREC WEEK | _mayo 2019_
    -   Colaboración en logística durante la ejecución del evento en la Cumbre de Redes y Almacenamiento


### Formación académica

-   **Ingeniería en Desarrollo Sustentable** · Tecnológico de Monterrey | _agosto 2017 – diciembre 2021_
    -   Promedio final de 91, beca de 50% por mérito académico
-   **Global Entrepreneurship Summer School** · En línea | _septiembre 2020_
    -   Curso intensivo de participación internacional emprendimiento enfocado en sustentabilidad energética
-   **Curso de paneles interconectados a la red de la CCEEA** · Monterrey, México | _noviembre 2019_
    -   Capacitación en diseño, valoración e instalación de sistemas fotovoltaicos residenciales y comerciales


### Habilidades técnicas

-   **Programación:** .NET Core · Python
-   **Software:** SketchUp · Helioscope · PowerWorld · Excel · Sistemas de Información Geográfica
-   **Cálculos:** Evaluaciones financieras · Balances de materia y energía · Sistemas de transmisión eléctrica


### Competencias

-   **Idiomas:** Inglés B2
-   **Aptitudes:** Proactividad y asertividad · Comunicación y negociación · Flexibilidad · Facilidad de aprendizaje
-   **Intereses:** Responsabilidad social y ambiental · Sustentabilidad energética y cambio climático